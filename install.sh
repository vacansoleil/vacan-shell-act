#!/usr/bin/env bash
sudo cp de_escape /usr/local/bin/
sudo cp gitclean /usr/local/bin/
sudo cp hide_secrets /usr/local/bin/
sudo cp json2yaml /usr/local/bin/
sudo cp json_from_dir /usr/local/bin/
sudo cp show_secrets /usr/local/bin/
sudo cp stripit /usr/local/bin/
sudo cp un_quote /usr/local/bin/
sudo cp yaml2json /usr/local/bin/